# QKD networks signal distribution

Library for comparing different solutions of optimal signal distribution task (task's russian description at https://www.notion.so/5e9725ae4ea04221af24d0d170b9ad62) 

## Dependecies

Main dependency is PuLP library for linear programming solution: https://pypi.org/project/PuLP/

## Usage example

```python
import time
from tqdm import trange

import numpy as np

from qkd_networks_signal_distribution.problem_generator import generate_problem
from qkd_networks_signal_distribution.linear_programming_solver import linear_programming_solution
from qkd_networks_signal_distribution.greedy_solution import greedy_solution
from qkd_networks_signal_distribution.solution_checker import check_solution


nodes_count = 30
edges_to_nodes_ratio = 2
pathes_count_constant = 1


repeat_iterations_count = 2

error_sum = 0
generation_time_sum = 0
linear_time_sum = 0
greedy_time_sum = 0

minimal_error = 1
maximal_error = 0

for iteration_idx in range(repeat_iterations_count):
    print("Iteration idx:", iteration_idx)
    start_time = time.time()
    graph, goal = generate_problem(nodes_count, edges_to_nodes_ratio, pathes_count_constant)
    generation_time_sum += time.time() - start_time

    start_time = time.time()
    linear_flow, flow_edges = linear_programming_solution(graph, goal)
    linear_time_sum += time.time() - start_time
    linear_resulted_flow = check_solution(graph, goal, linear_flow, flow_edges)
    assert np.amax(goal - linear_resulted_flow) < 1e-6, "Error: linear solver didn't find an optimal solution"

    start_time = time.time()
    greedy_flow, flow_edges = greedy_solution(graph, goal)
    greedy_time_sum += time.time() - start_time
    greedy_resulted_flow = check_solution(graph, goal, greedy_flow, flow_edges)

    current_error = np.amax(goal - greedy_resulted_flow) / np.amax(goal)
    error_sum += current_error
    if minimal_error > current_error:
        minimal_error = current_error
    if maximal_error < current_error:
        maximal_error = current_error
    
mean_errors = error_sum / repeat_iterations_count
maximal_errors = maximal_error
minimal_errors = minimal_error

mean_generation_time = generation_time_sum / repeat_iterations_count
mean_linear_programming_time = linear_time_sum / repeat_iterations_count
mean_greedy_time = greedy_time_sum / repeat_iterations_count

```


