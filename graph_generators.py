import numpy as np
from collections import namedtuple


Graph = namedtuple('Graph', ['nodes_count', 'edges'])
Edge = namedtuple('Edge', ['id', 'u', 'v', 'dim'])


def fully_connected_graph(nodes_count: int, max_dim: int, seed: int = 0,
                          fixed_dim: bool = False, log_dim: bool = False) -> Graph:
    rng = np.random.RandomState(seed)
    graph = Graph(nodes_count=nodes_count, edges=[])
    edges_count = 0
    for u in range(nodes_count):
        for v in range(u + 1, nodes_count):
            dim = max_dim
            if not fixed_dim:
                dim = rng.randint(max_dim) + 1
            if log_dim:
                dim = np.log(dim)
            graph.edges.append(Edge(edges_count, u, v, dim))
            edges_count += 1
    return graph


def tree_graph(nodes_count: int, max_dim: int, seed: int = 0, fixed_dim: bool = False, log_dim: bool = False):
    rng = np.random.RandomState(seed)
    graph = Graph(nodes_count=nodes_count, edges=[])
    edges_count = 0
    for u in range(1, nodes_count):
        dim = max_dim
        if not fixed_dim:
            dim = rng.randint(max_dim) + 1
        if log_dim:
            dim = np.log(dim)
        graph.edges.append(Edge(edges_count, rng.randint(u), u, dim))
        edges_count += 1
    return graph


def random_connected_graph(nodes_count: int, edges_count: int, max_dim: int, seed: int = 0,
                           fixed_dim: bool = False, log_dim: bool = False):
    rng = np.random.RandomState(seed)
    assert edges_count + 1 >= nodes_count
    assert 2 * edges_count <= nodes_count * (nodes_count - 1)
    tree = tree_graph(nodes_count, max_dim, seed, fixed_dim, log_dim)
    tree_edges = set()
    for edge in tree.edges:
        tree_edges.add((edge.u, edge.v))
        tree_edges.add((edge.v, edge.u))
    edges_to_sample_from = []
    for u in range(nodes_count):
        for v in range(u + 1, nodes_count):
            if not ((u, v) in tree_edges):
                edges_to_sample_from.append((u, v))
    rng.shuffle(edges_to_sample_from)
    all_edges = []
    for edge in tree.edges:
        all_edges.append(edge)
    edges_count -= nodes_count - 1
    for i in range(edges_count):
        u, v = edges_to_sample_from[i]
        dim = max_dim
        if not fixed_dim:
            dim = rng.randint(max_dim) + 1
        if log_dim:
            dim  = np.log(dim)
        all_edges.append(Edge(i + nodes_count - 1, u, v, dim))
    return Graph(nodes_count=nodes_count, edges=all_edges)
