from __future__ import annotations
from typing import Tuple, Dict, List
from collections import namedtuple
from dataclasses import dataclass

import numpy as np
import random

from qkd_networks_signal_distribution.utils import Graph, Edge


@dataclass
class FlowEdge:
    u: int
    v: int
    capacity: int
    flow: int
    original_edge_idx: int
    reverse_edge_reference: FlowEdge = None

    def free_space(self) -> int:
        return self.capacity - self.flow


FlowGraph = namedtuple('FlowGraph', ['nodes_count', 'edges'])


def get_flow_edge(u: int, v: int, capacity: int, edge_idx: int) -> Tuple[FlowEdge, FlowEdge]:
    flow_edge = FlowEdge(u=u, v=v, capacity=capacity, flow=0, original_edge_idx=edge_idx)
    reverse_flow_edge = FlowEdge(u=v, v=u, capacity=0, flow=0, original_edge_idx=edge_idx)
    flow_edge.reverse_edge_reference = reverse_flow_edge
    reverse_flow_edge.reverse_edge_reference = flow_edge
    return flow_edge, reverse_flow_edge


def graph_to_extended_flow_graph(graph: Graph, is_edge_valid: np.ndarray) -> FlowGraph:
    resulted_flow_graph = FlowGraph(nodes_count=2 * graph.nodes_count, edges=[])
    for edge_idx, edge in enumerate(graph.edges):
        if not is_edge_valid[edge_idx]:
            continue
        flow_edge, reverse_flow_edge = get_flow_edge(2 * edge.u + 1, 2 * edge.v, 1, edge_idx)
        resulted_flow_graph.edges.append(flow_edge)
        resulted_flow_graph.edges.append(reverse_flow_edge)
        flow_edge, reverse_flow_edge = get_flow_edge(2 * edge.v + 1, 2 * edge.u, 1, edge_idx + len(graph.edges))
        resulted_flow_graph.edges.append(flow_edge)
        resulted_flow_graph.edges.append(reverse_flow_edge)
    for node_idx in range(graph.nodes_count):
        node_edge, reverse_node_edge = get_flow_edge(2 * node_idx, 2 * node_idx + 1, 1, -1)
        resulted_flow_graph.edges.append(node_edge)
        resulted_flow_graph.edges.append(reverse_node_edge)
    return resulted_flow_graph


def get_flow_graph_adj(flow_graph: FlowGraph, rng: random.Random) -> Dict[int, List[FlowEdge]]:
    def add_edge(adj: Dict[int, List[FlowEdge]], flow_edge: FlowEdge):
        u = flow_edge.u
        if not (u in adj):
            adj[u] = []
        adj[u].append(flow_edge)

    adj_dict = {}

    for flow_edge in flow_graph.edges:
        add_edge(adj_dict, flow_edge)

    for key in adj_dict:
        rng.shuffle(adj_dict[key])

    return adj_dict


def find_one_flow_path(current_vertex: int, finish: int, adj_dict: Dict[int, List[FlowEdge]],
                         is_vertex_visited: np.ndarray):
    if current_vertex == finish:
        return True
    is_vertex_visited[current_vertex] = True
    for flow_edge in adj_dict[current_vertex]:
        if (flow_edge.free_space() <= 0) or (is_vertex_visited[flow_edge.v]):
            continue
        if find_one_flow_path(flow_edge.v, finish, adj_dict, is_vertex_visited):
            flow_edge.flow += 1
            flow_edge.reverse_edge_reference.flow -= 1
            return True
    return False


def find_flow(flow_graph: FlowGraph, start: int, finish: int, pathes_count: int, rng: random.Random) \
        -> Tuple[bool, FlowGraph]:
    adj_dict = get_flow_graph_adj(flow_graph, rng)
    start = 2 * start + 1
    finish = 2 * finish

    for _ in range(pathes_count):
        is_vertex_visited = np.zeros(flow_graph.nodes_count, dtype=int)
        is_vertex_visited[start - 1] = True  # To prevent cycles through start vertex
        if not find_one_flow_path(start, finish, adj_dict, is_vertex_visited):
            return False, flow_graph

    return True, flow_graph


def find_edges_in_flow(graph: Graph, start: int, finish: int, is_edge_valid: np.ndarray, \
                       pathes_count: int, rng: random.Random) -> Tuple[bool, List[int]]:
    flow_graph = graph_to_extended_flow_graph(graph, is_edge_valid)
    are_pathes_founded, renewed_flow_graph = find_flow(flow_graph, start, finish, pathes_count, rng)

    if not are_pathes_founded:
        return False, None

    edges_idx = []
    for flow_edge in flow_graph.edges:
        if (flow_edge.capacity <= 0) or (flow_edge.flow <= 0) or (flow_edge.original_edge_idx < 0):
            continue
        edges_idx.append(flow_edge.original_edge_idx)

    return True, edges_idx
