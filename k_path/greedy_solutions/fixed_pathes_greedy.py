from __future__ import annotations
from typing import Dict, List, Tuple
from collections import namedtuple
from dataclasses import dataclass

import numpy as np
import copy
import random

from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes
from qkd_networks_signal_distribution.k_path.solution_converter import convert_solution
from qkd_networks_signal_distribution.k_path.flow_graph import find_edges_in_flow


def greedy_solution(graph: Graph, goal: np.ndarray,
                    pathes_count: int, seed: int = 0) -> Tuple[float, np.ndarray, List[OneIterationPathes]]:
    rng = random.Random(seed)
    nodes_count = graph.nodes_count
    initial_graph = graph
    graph = copy.deepcopy(graph)

    flow_source_sink_pairs = [(s, t) for t in range(nodes_count) for s in range(t)]
    flow_source_sink_pairs = sorted(flow_source_sink_pairs, key=lambda x: goal[x[0]][x[1]], reverse=True)
    flow_edges = [(str(e.u), str(e.v)) for e in graph.edges] + [(str(e.v), str(e.u)) for e in graph.edges]

    solution = np.zeros((len(flow_source_sink_pairs), len(flow_edges)), dtype=int)
    edges_usage_count = np.zeros(len(graph.edges), dtype=int)
    current_flow_level = np.amax(goal)

    for iteration in range(len(flow_source_sink_pairs) + 1):

        if (iteration > 0) and (current_flow_level > 0):
            new_flow_level = 0
            if iteration < len(flow_source_sink_pairs):
                start, finish = flow_source_sink_pairs[iteration]
                new_flow_level = goal[start][finish]
            flow_level_to_push = current_flow_level - new_flow_level
            if flow_level_to_push > 1e-9:
                available_level_to_push = flow_level_to_push
                for edge_idx, edge in enumerate(graph.edges):
                    flow_to_push = flow_level_to_push * edges_usage_count[edge_idx]
                    if (flow_to_push > edge.dim) and (edges_usage_count[edge_idx] > 0):
                        available_level_to_push = min(available_level_to_push, edge.dim / edges_usage_count[edge_idx])
                for edge_idx, edge in enumerate(graph.edges):
                    flow_to_push = available_level_to_push * edges_usage_count[edge_idx]
                    graph.edges[edge_idx] = edge._replace(dim=edge.dim - flow_to_push)
                if available_level_to_push < flow_level_to_push:
                    current_flow_level = current_flow_level - available_level_to_push
                    break
                current_flow_level = new_flow_level

        if iteration < len(flow_source_sink_pairs):
            start, finish = flow_source_sink_pairs[iteration]

            # Binary search by the number of largest edges needed to find a path.
            # Value len(sorted_edges_idx) + 1 means that there is no valid path in the whole graph.

            sorted_edges_idx = np.arange(len(graph.edges), dtype=int).tolist()
            sorted_edges_idx = sorted(sorted_edges_idx,
                                      key=lambda idx: graph.edges[idx].dim / (edges_usage_count[idx] + 1), reverse=True)

            left_border = 1
            right_border = len(sorted_edges_idx) + 1
            while left_border < right_border:
                middle = (left_border + right_border) // 2
                is_edge_valid = np.zeros(len(graph.edges), dtype=bool)
                is_edge_valid[sorted_edges_idx[:middle]] = True
                are_paths_found, _ = find_edges_in_flow(graph, start, finish, is_edge_valid, pathes_count, rng)
                if are_paths_found:
                    right_border = middle
                else:
                    left_border = middle + 1
            if left_border == len(sorted_edges_idx) + 1:
                break
            if graph.edges[sorted_edges_idx[left_border - 1]].dim < 1e-9:
                break

            is_edge_valid = np.zeros(len(graph.edges), dtype=bool)
            is_edge_valid[sorted_edges_idx[:left_border]] = True
            are_paths_found, current_flow_edges_idx = find_edges_in_flow(graph, start, finish,
                                                                         is_edge_valid, pathes_count, rng)
            assert are_paths_found, "Error: Pathes should be found!"
            for edge_idx in current_flow_edges_idx:
                solution[iteration][edge_idx] = 1
                if edge_idx >= len(graph.edges):
                    edge_idx -= len(graph.edges)
                edges_usage_count[edge_idx] += 1

    return convert_solution(initial_graph, goal, current_flow_level, solution,
                            flow_source_sink_pairs, flow_edges, pathes_count)
