from __future__ import annotations
from typing import Tuple, Dict, List
from collections import namedtuple
from dataclasses import dataclass

import numpy as np
import copy
import random

from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes
from qkd_networks_signal_distribution.solution_checker import check_solution
from qkd_networks_signal_distribution.k_path.solution_converter import convert_flow_into_pathes
from qkd_networks_signal_distribution.k_path.flow_graph import find_edges_in_flow


def edge_validation(edge: Edge, lower_bound: float):
    return edge.dim >= lower_bound


def greedy_solution(graph: Graph, goal: np.ndarray, pathes_count: int,
                    one_iteration_flow: float = 1.0, logarithm_mode: bool = False,
                    logarithm_mode_coefficient: float = 0.5, seed: int = 0) -> Tuple[float, np.ndarray,
                                                                                     List[OneIterationPathes]]:
    rng = random.Random(seed)
    current_result = np.zeros((graph.nodes_count, graph.nodes_count), dtype=float)
    initial_graph = graph
    graph = copy.deepcopy(graph)

    flow_edges = [(str(e.u), str(e.v)) for e in graph.edges] + [(str(e.v), str(e.u)) for e in graph.edges]

    solution = []

    while True:
        flow_left_to_push = goal - current_result
        start, finish = np.unravel_index(np.argmax(flow_left_to_push, axis=None), flow_left_to_push.shape)

        sorted_edges_idx = np.arange(len(graph.edges), dtype=int).tolist()
        sorted_edges_idx = sorted(sorted_edges_idx, key=lambda idx: graph.edges[idx].dim, reverse=True)

        # Binary search by the number of largest edges needed to find a path.
        # Value len(sorted_edges_idx) + 1 means that there is no valid path in the whole graph.

        left_border = 1
        right_border = len(sorted_edges_idx) + 1
        while left_border < right_border:
            middle = (left_border + right_border) // 2
            lowerbound_to_check = graph.edges[sorted_edges_idx[middle - 1]].dim
            is_edge_valid = np.array([edge_validation(edge, lowerbound_to_check) for edge in graph.edges], dtype=bool)
            are_pathes_found, _ = find_edges_in_flow(graph, start, finish, is_edge_valid, pathes_count, rng)
            if are_pathes_found:
                right_border = middle
            else:
                left_border = middle + 1
        if left_border == len(sorted_edges_idx) + 1:
            break
        edge_dim_lowerbound = graph.edges[sorted_edges_idx[left_border - 1]].dim
        if edge_dim_lowerbound < 1e-9:
            break
        if logarithm_mode:
            flow_to_push = min(edge_dim_lowerbound, flow_left_to_push[start][finish]) * logarithm_mode_coefficient
            if flow_to_push < one_iteration_flow:
                flow_to_push /= logarithm_mode_coefficient
        else:
            flow_to_push = min(min(edge_dim_lowerbound, one_iteration_flow), flow_left_to_push[start][finish])
        if flow_to_push < 1e-9:
            break
        current_result[start][finish] += flow_to_push
        is_edge_valid = np.array([edge_validation(edge, edge_dim_lowerbound)
                                  for edge in graph.edges], dtype=bool)
        _, edges_idx = find_edges_in_flow(graph, start, finish, is_edge_valid, pathes_count, rng)

        edges_usage_count = np.zeros(len(flow_edges), dtype=int)
        for edge_idx in edges_idx:
            unordered_edge_idx = edge_idx
            if unordered_edge_idx >= len(graph.edges):
                unordered_edge_idx -= len(graph.edges)
            current_edge = graph.edges[unordered_edge_idx]
            graph.edges[unordered_edge_idx] = current_edge._replace(dim=current_edge.dim - flow_to_push)
            edges_usage_count[edge_idx] = 1

        solution.append(
            convert_flow_into_pathes(graph.nodes_count, start, finish, flow_to_push, edges_usage_count, pathes_count,
                                     flow_edges))

    resulted_flow = check_solution(initial_graph, goal, solution, pathes_count)
    return np.amax(goal - resulted_flow), resulted_flow, solution
