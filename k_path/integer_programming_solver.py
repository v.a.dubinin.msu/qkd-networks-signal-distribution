import numpy as np
from typing import List, Tuple

from pulp import *

from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes
from qkd_networks_signal_distribution.k_path.solution_converter import convert_solution


def get_adj_revadj_integer(graph: Graph) -> Tuple[dict, dict]:
    def add_edge(adj, u, v):
        if not (u in adj):
            adj[u] = []
        adj[u].append(v)

    adj_list = {}
    revadj_list = {}
    for edge in graph.edges:
        add_edge(adj_list, edge.u, edge.v)
        add_edge(adj_list, edge.v, edge.u)
        add_edge(revadj_list, edge.v, edge.u)
        add_edge(revadj_list, edge.u, edge.v)

    return adj_list, revadj_list


def goal_checking_integer_programming_solution(graph: Graph, goal: np.ndarray, minimal_pathes_count: int) \
        -> Tuple[bool, float, np.ndarray, List[Tuple[str, str]], List[Tuple[str, str]]]:
    nodes_count = graph.nodes_count
    adj_list, revadj_list = get_adj_revadj_integer(graph)

    prob = LpProblem("QKD", LpMaximize)

    flow_source_sink_pairs = [(str(s), str(t)) for t in range(nodes_count) for s in range(t)]
    flow_edges = [(str(e.u), str(e.v)) for e in graph.edges] + [(str(e.v), str(e.u)) for e in graph.edges]
    goal_edges = [((source, sink), (source, str(v))) for source, sink in flow_source_sink_pairs for v in
                  adj_list[int(source)]]

    vars = LpVariable.dicts("Flow", (flow_source_sink_pairs, flow_edges), 0, 1, cat='Integer')

    prob += (
        lpSum([vars[source][edge] for (source, edge) in goal_edges]),
        "Goal function",
    )

    for edge in graph.edges:
        prob += (
            lpSum([goal[int(source)][int(sink)] * vars[(source, sink)][(str(edge.u), str(edge.v))] for source, sink in
                   flow_source_sink_pairs]
                  + [goal[int(source)][int(sink)] * vars[(source, sink)][(str(edge.v), str(edge.u))] for source, sink in
                     flow_source_sink_pairs]) <= edge.dim,
            "Edge_{0}_capacity_limitation".format((str(edge.u), str(edge.v)))
        )
        for t in range(nodes_count):
            if t > edge.v:
                prob += (
                    vars[(str(edge.v), str(t))][(str(edge.u), str(edge.v))] <= 0,
                    "Sink_{0}_Edge_{1}_source_incoming_prohibition".format(t, (edge.u, edge.v))
                )
            if t > edge.u:
                prob += (
                    vars[(str(edge.u), str(t))][(str(edge.v), str(edge.u))] <= 0,
                    "Sink_{0}_Edge_{1}_source_incoming_prohibition".format(t, (edge.v, edge.u))
                )

    for source, sink in flow_source_sink_pairs:
        s = int(source)
        t = int(sink)
        prob += (
            lpSum([vars[(source, sink)][(source, str(v))] for v in adj_list[s]]) == minimal_pathes_count,
            "Flow between {0} minimal pathes count restriction".format((source, sink))
        )
        for u in range(nodes_count):
            if (u == s) or (u == t):
                continue
            prob += (
                lpSum([vars[(source, sink)][(str(u), str(v))] for v in adj_list[u]] + [
                    -vars[(source, sink)][(str(v), str(u))] for v in revadj_list[u]]) == 0,
                "Flow between {0} conservation in vertex {1}".format((source, sink), u)
            )
            prob += (
                lpSum([vars[(source, sink)][(str(u), str(v))] for v in adj_list[u]]) <= 1,
                "Flow between {0} vertex {1} one path restriction".format((source, sink), u)
            )

    status = prob.solve()

    solution = [[value(vars[source_sink][e]) for e in flow_edges] for source_sink in flow_source_sink_pairs]
    return status == 1, 0.0, solution, flow_source_sink_pairs, flow_edges


def integer_programming_solution(graph: Graph, goal: np.ndarray, minimal_pathes_count: int) \
        -> Tuple[float, np.ndarray, List[OneIterationPathes]]:
    left_border = 0
    right_border = np.amax(goal)
    while right_border - left_border > 1e-5:
        middle = (left_border + right_border) / 2
        is_solution_found, _, _, _, _ = goal_checking_integer_programming_solution(graph,
                                                                                   np.maximum(0, goal - middle),
                                                                                   minimal_pathes_count)
        if is_solution_found:
            right_border = middle
        else:
            left_border = middle
    _, _, solution, flow_source_sink_pairs, flow_edges = goal_checking_integer_programming_solution(
        graph,
        np.maximum(0, goal - right_border),
        minimal_pathes_count
    )
    flow_level, resulted_flow, converted_solution = convert_solution(graph, goal, right_border, solution,
                                                                     flow_source_sink_pairs, flow_edges,
                                                                     minimal_pathes_count)
    return flow_level, resulted_flow, converted_solution
