from __future__ import annotations

from typing import Tuple, Dict, List
from collections import namedtuple
import random
import numpy as np

from qkd_networks_signal_distribution.graph_generators import random_connected_graph
from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes
from qkd_networks_signal_distribution.k_path.flow_graph import find_edges_in_flow
from qkd_networks_signal_distribution.k_path.solution_converter import convert_flow_into_pathes

from dataclasses import dataclass


#
# Here we use only graph's topology as input and change graph's edges capacities in output.
#
def process_one_pair(graph: Graph, goal: np.ndarray, start: int, finish: int, pathes_count: int, goal_upper_bound: int,
                     rng: np.random.RandomState) -> Tuple[bool, OneIterationPathes]:
    flow_size = rng.randint(goal_upper_bound) + 1
    are_pathes_found, edges_in_flow = find_edges_in_flow(graph, start, finish, np.full(len(graph.edges), True),
                                                         pathes_count, rng)
    if not are_pathes_found:
        return False, None

    flow_edges = [(str(e.u), str(e.v)) for e in graph.edges] + [(str(e.v), str(e.u)) for e in graph.edges]
    edges_usage_count = np.zeros(len(flow_edges), dtype=int)
    for edge_idx in edges_in_flow:
        unordered_edge_idx = edge_idx
        if unordered_edge_idx >= len(graph.edges):
            unordered_edge_idx -= len(graph.edges)
        current_edge = graph.edges[unordered_edge_idx]
        graph.edges[unordered_edge_idx] = current_edge._replace(dim=current_edge.dim + flow_size)
        edges_usage_count[edge_idx] = 1

    one_iteration_paths = convert_flow_into_pathes(graph.nodes_count, start, finish, flow_size,
                                                   edges_usage_count, pathes_count, flow_edges)
    goal[start][finish] += flow_size

    return True, one_iteration_paths


def generate_goal(graph: Graph, pathes_count: int, goal_upper_bound: int, seed: int, \
                  generation_pathes_count: int, multiple_pathes_mode: bool) -> Tuple[Graph, np.ndarray,
                                                                                     List[OneIterationPathes], bool]:
    rng = np.random.RandomState(seed)
    nodes_count = graph.nodes_count
    goal = np.zeros((nodes_count, nodes_count))
    for edge_idx in range(len(graph.edges)):
        graph.edges[edge_idx] = graph.edges[edge_idx]._replace(dim=0)

    find_all_pathes = True
    pathes_list = []
    if multiple_pathes_mode:
        for _ in range(generation_pathes_count):
            st, fn = rng.choice(np.arange(nodes_count), size=2, replace=False)
            if st > fn:
                st, fn = fn, st
            are_paths_found, one_iteration_paths = process_one_pair(graph, goal, st, fn, pathes_count,
                                                                    goal_upper_bound, rng)
            if not are_paths_found:
                find_all_pathes = False
                break
            pathes_list.append(one_iteration_paths)
    else:
        for fn in range(nodes_count):
            for st in range(fn):
                are_paths_found, one_iteration_paths = process_one_pair(graph, goal, st, fn, pathes_count,
                                                                        goal_upper_bound, rng)
                if not are_paths_found:
                    find_all_pathes = False
                    break
                pathes_list.append(one_iteration_paths)

    return graph, goal, pathes_list, find_all_pathes


def generate_problem(nodes_count: int, edges_to_nodes_ratio: float, pathes_count: int,
                     seed: int = 0, generation_pathes_count_constant: float = 1.0,
                     multiple_pathes_mode: bool = False, goal_upper_bound: int = 100) -> Tuple[Graph, np.ndarray,
                                                                                               List[OneIterationPathes]]:
    successful_problem_generation = False
    rng = np.random.RandomState(seed)
    candidate_seed = rng.randint(1e9)
    while not successful_problem_generation:
        edges_count = int(edges_to_nodes_ratio * nodes_count + 0.5)
        generation_pathes_count = int(generation_pathes_count_constant * nodes_count * nodes_count + 0.5)
        graph = random_connected_graph(nodes_count=nodes_count, edges_count=edges_count, max_dim=1, seed=candidate_seed,
                                       fixed_dim=False, log_dim=False)
        graph, goal, pathes_list, result = generate_goal(graph, pathes_count=pathes_count, goal_upper_bound=goal_upper_bound,
                                                         seed=candidate_seed, generation_pathes_count=generation_pathes_count,
                                                         multiple_pathes_mode=multiple_pathes_mode)
        if result:
            successful_problem_generation = True
            # print("Successful generatation graph with seed {0}".format(seed))
        else:
            print("Fail to generate graph with seed {0}".format(candidate_seed))
            candidate_seed = rng.randint(1e9)
    return graph, goal, pathes_list
