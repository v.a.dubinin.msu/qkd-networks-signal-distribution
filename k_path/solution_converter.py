import numpy as np
from typing import List, Dict, Tuple

from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes
from qkd_networks_signal_distribution.solution_checker import check_solution
from dataclasses import dataclass


@dataclass
class EdgeWithCapacity:
    v: int
    capacity: float
    edge_idx: int


def find_path_with_capacity(v: int, finish: int, adj_matrix: Dict[int, List[EdgeWithCapacity]],
                            visited_nodes: np.ndarray) -> Tuple[bool, List[EdgeWithCapacity]]:
    if v == finish:
        return True, []
    visited_nodes[v] = 1
    if not (v in adj_matrix):
        return False, []
    for flow_edge in adj_matrix[v]:
        to = flow_edge.v
        if (flow_edge.capacity < 0.5) or (visited_nodes[to] == 1):
            continue
        is_found, result = find_path_with_capacity(to, finish, adj_matrix, visited_nodes)
        if is_found:
            return True, [flow_edge] + result
    return False, []


def convert_flow_into_pathes(nodes_count: int,
                             start: int,
                             finish: int,
                             flow_to_push: float,
                             solution: np.ndarray,
                             pathes_count: int,
                             flow_edges: List[Tuple[str, str]]) -> OneIterationPathes:
    adj_matrix = {}
    for edge_idx, (edge, capacity) in enumerate(zip(flow_edges, solution)):
        u, v = int(edge[0]), int(edge[1])
        if not (u in adj_matrix):
            adj_matrix[u] = []
        adj_matrix[u].append(EdgeWithCapacity(v=v, capacity=capacity, edge_idx=edge_idx))
    edges_count = len(flow_edges) // 2
    all_pathes = []
    for _ in range(pathes_count):
        visited_nodes = np.zeros(nodes_count, dtype=int)
        is_found, flow_edges_path = find_path_with_capacity(start, finish, adj_matrix, visited_nodes)
        assert is_found
        pathes_edge_idx = []
        for i, flow_edge in enumerate(flow_edges_path):
            flow_edge.capacity -= 1
            unordered_edge_idx = flow_edge.edge_idx
            if unordered_edge_idx >= edges_count:
                unordered_edge_idx -= edges_count
            pathes_edge_idx.append(unordered_edge_idx)
        all_pathes.append(pathes_edge_idx)
    return OneIterationPathes(signal_size=flow_to_push, start=start, finish=finish, pathes_edge_idx=all_pathes)


def convert_solution(graph: Graph, goal: np.ndarray, current_flow_level: float,
                     solution: np.ndarray, flow_source_sink_pairs: List[Tuple[int, int]],
                     flow_edges: List[Tuple[str, str]], pathes_count: int) -> Tuple[float,
                                                                                    np.ndarray,
                                                                                    List[OneIterationPathes]]:
    nodes_count = graph.nodes_count
    converted_solution = []
    for i, (start, finish) in enumerate(flow_source_sink_pairs):
        start, finish = int(start), int(finish)
        flow_to_push = max(0, goal[start][finish] - current_flow_level)
        if flow_to_push < 1e-6:
            continue
        converted_solution.append(convert_flow_into_pathes(nodes_count, start, finish, flow_to_push,
                                                           solution[i], pathes_count, flow_edges))

    resulted_flow = check_solution(graph, goal, converted_solution, pathes_count)
    return np.amax(goal - resulted_flow), resulted_flow, converted_solution
