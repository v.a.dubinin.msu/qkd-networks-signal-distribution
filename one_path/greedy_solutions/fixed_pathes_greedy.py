from __future__ import annotations
from typing import Dict, List, Tuple

import numpy as np
import copy
import random

from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes
from qkd_networks_signal_distribution.solution_checker import check_solution


def find_random_path_greedy(current_vertex: int, finish: int, adj_dict: Dict[int, List[Tuple[int, int]]],
                            is_vertex_visited: np.ndarray) -> Tuple[bool, List[int]]:
    if current_vertex == finish:
        return True, []
    is_vertex_visited[current_vertex] = True
    if not (current_vertex in adj_dict):
        return False, []
    for to, edge_idx in adj_dict[current_vertex]:
        if is_vertex_visited[to]:
            continue
        is_path_found, current_path = find_random_path_greedy(to, finish, adj_dict, is_vertex_visited)
        if is_path_found:
            return True, [edge_idx] + current_path
    return False, []


def find_valid_path(graph: Graph, start: int, finish: int, is_edge_valid: np.ndarray, rng) -> Tuple[bool, List[int]]:
    adj_matrix = {}
    for edge_idx, edge in enumerate(graph.edges):
        if is_edge_valid[edge_idx]:
            u, v = edge.u, edge.v

            def add(v, to, edge_idx):
                if not (v in adj_matrix):
                    adj_matrix[v] = []
                adj_matrix[v].append((to, edge_idx))

            add(u, v, edge_idx)
            add(v, u, edge_idx)

    for key in adj_matrix:
        rng.shuffle(adj_matrix[key])

    is_vertex_visited = np.zeros(graph.nodes_count, dtype=int)
    is_path_found, edges_idx = find_random_path_greedy(start, finish, adj_matrix, is_vertex_visited)

    if not is_path_found:
        return False, []

    return True, edges_idx


def greedy_solution(graph: Graph, goal: np.ndarray, seed: int = 0) -> Tuple[float, np.ndarray, List[OneIterationPathes]]:
    rng = random.Random(seed)
    nodes_count = graph.nodes_count
    initial_graph = graph
    graph = copy.deepcopy(graph)

    flow_source_sink_pairs = [(s, t) for t in range(nodes_count) for s in range(t)]
    flow_source_sink_pairs = sorted(flow_source_sink_pairs, key=lambda x: goal[x[0]][x[1]], reverse=True)

    solution = []
    edges_usage_count = np.zeros(len(graph.edges), dtype=int)
    current_flow_level = np.amax(goal)
    for iteration in range(len(flow_source_sink_pairs) + 1):
        if (iteration > 0) and (current_flow_level > 0):
            new_flow_level = 0
            if iteration < len(flow_source_sink_pairs):
                start, finish = flow_source_sink_pairs[iteration]
                new_flow_level = goal[start][finish]
            flow_level_to_push = current_flow_level - new_flow_level
            if flow_level_to_push > 1e-9:
                available_level_to_push = flow_level_to_push
                for edge_idx, edge in enumerate(graph.edges):
                    flow_to_push = flow_level_to_push * edges_usage_count[edge_idx]
                    if (flow_to_push > edge.dim) and (edges_usage_count[edge_idx] > 0):
                        available_level_to_push = min(available_level_to_push, edge.dim / edges_usage_count[edge_idx])
                for edge_idx, edge in enumerate(graph.edges):
                    flow_to_push = available_level_to_push * edges_usage_count[edge_idx]
                    graph.edges[edge_idx] = edge._replace(dim=edge.dim - flow_to_push)
                if available_level_to_push < flow_level_to_push:
                    current_flow_level = current_flow_level - available_level_to_push
                    break
                current_flow_level = new_flow_level
        if iteration < len(flow_source_sink_pairs):
            start, finish = flow_source_sink_pairs[iteration]

            # Binary search by the number of largest edges needed to find a path.
            # Value len(sorted_edges_idx) + 1 means that there is no valid path in the whole graph.

            sorted_edges_idx = np.arange(len(graph.edges), dtype=int).tolist()
            sorted_edges_idx = sorted(sorted_edges_idx,
                                      key=lambda idx: graph.edges[idx].dim / (edges_usage_count[idx] + 1), reverse=True)

            left_border = 1
            right_border = len(sorted_edges_idx) + 1
            while left_border < right_border:
                middle = (left_border + right_border) // 2
                is_edge_valid = np.zeros(len(graph.edges), dtype=bool)
                is_edge_valid[sorted_edges_idx[:middle]] = True
                is_path_found, _ = find_valid_path(graph, start, finish, is_edge_valid, rng)
                if is_path_found:
                    right_border = middle
                else:
                    left_border = middle + 1
            if left_border == len(sorted_edges_idx) + 1:
                break
            if graph.edges[sorted_edges_idx[left_border - 1]].dim < 1e-9:
                break
            is_edge_valid = np.zeros(len(graph.edges), dtype=bool)
            is_edge_valid[sorted_edges_idx[:left_border]] = True

            is_path_found, path_edges_idx = find_valid_path(graph, start, finish, is_edge_valid, rng)
            assert is_path_found, "Error: Pathes should be found!"

            for edge_idx in path_edges_idx:
                edges_usage_count[edge_idx] += 1

            solution.append(OneIterationPathes(start=start, finish=finish, signal_size=current_flow_level,
                                               pathes_edge_idx=[path_edges_idx]))
    for idx, path in enumerate(solution):
        solution[idx] = path._replace(signal_size=path.signal_size - current_flow_level)
    resulted_flow = check_solution(initial_graph, goal, solution)
    return current_flow_level, resulted_flow, solution
