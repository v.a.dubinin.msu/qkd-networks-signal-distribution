import numpy as np
from typing import Tuple, List
import copy
import random

from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes
from qkd_networks_signal_distribution.solution_checker import check_solution


def get_adj_greedy(graph: Graph, rng) -> dict:
    def add_edge(adj: dict, u: int, idx: int):
        if not (u in adj):
            adj[u] = []
        adj[u].append(idx)

    adj_matrix = {}

    for idx, edge in enumerate(graph.edges):
        add_edge(adj_matrix, edge.u, idx)
        add_edge(adj_matrix, edge.v, idx + len(graph.edges))

    for key in adj_matrix:
        rng.shuffle(adj_matrix[key])

    return adj_matrix


def get_other_vertex(graph: Graph, edge_idx: int, first_vertex: int) -> int:
    return graph.edges[edge_idx].u + graph.edges[edge_idx].v - first_vertex


def find_path_with_lowerbounded_capacities(current_vertex: int, finish: int, is_edge_valid: np.ndarray,
                                           graph: Graph, adj: dict, visited_vertices: np.ndarray) \
        -> Tuple[bool, List[int]]:
    if current_vertex == finish:
        return True, []
    visited_vertices[current_vertex] = True
    for edge_idx in adj[current_vertex]:
        unordered_edge_idx = edge_idx
        if unordered_edge_idx >= len(graph.edges):
            unordered_edge_idx -= len(graph.edges)
        if not is_edge_valid[unordered_edge_idx]:
            continue
        next_vertex = get_other_vertex(graph, unordered_edge_idx, current_vertex)
        if visited_vertices[next_vertex]:
            continue
        path_is_found, path = find_path_with_lowerbounded_capacities(next_vertex, finish, is_edge_valid,
                                                                     graph, adj, visited_vertices)
        if path_is_found:
            return path_is_found, path + [edge_idx]
    return False, []


def edge_validation(edge: Edge, lower_bound: float):
    return edge.dim >= lower_bound


def greedy_solution(graph: Graph, goal: np.ndarray, one_iteration_flow: float = 1.0,
                    logarithm_mode: bool = False, logarithm_mode_coefficient: float = 0.5,
                    seed: int = 0) -> Tuple[float, np.ndarray, List[OneIterationPathes]]:
    rng = random.Random(seed)
    nodes_count = graph.nodes_count
    current_result = np.zeros((nodes_count, nodes_count), dtype=float)
    initial_graph = graph
    graph = copy.deepcopy(graph)

    adj = get_adj_greedy(graph, rng)
    solution = []

    while True:
        flow_left_to_push = goal - current_result
        start, finish = np.unravel_index(np.argmax(flow_left_to_push, axis=None), flow_left_to_push.shape)

        sorted_edges_idx = np.arange(len(graph.edges), dtype=int).tolist()
        sorted_edges_idx = sorted(sorted_edges_idx, key=lambda idx: graph.edges[idx].dim, reverse=True)

        # Binary search by the number of largest edges needed to find a path.
        # Value len(sorted_edges_idx) + 1 means that there is no valid path in the whole graph.

        left_border = 1
        right_border = len(sorted_edges_idx) + 1
        while left_border < right_border:
            middle = (left_border + right_border) // 2
            lowerbound_to_check = graph.edges[sorted_edges_idx[middle - 1]].dim
            is_edge_valid = np.array([edge_validation(edge, lowerbound_to_check) for edge in graph.edges], dtype=bool)
            visited_vertices = np.zeros(nodes_count, dtype=bool)
            is_path_found, _ = find_path_with_lowerbounded_capacities(start, finish, is_edge_valid, graph,
                                                                         adj, visited_vertices)
            if is_path_found:
                right_border = middle
            else:
                left_border = middle + 1
        if left_border == len(sorted_edges_idx) + 1:
            break
        edge_dim_lowerbound = graph.edges[sorted_edges_idx[left_border - 1]].dim
        if edge_dim_lowerbound < 1e-9:
            break
        if logarithm_mode:
            flow_to_push = min(edge_dim_lowerbound, flow_left_to_push[start][finish]) * logarithm_mode_coefficient
            if flow_to_push < one_iteration_flow:
                flow_to_push /= logarithm_mode_coefficient
        else:
            flow_to_push = min(min(edge_dim_lowerbound, one_iteration_flow), flow_left_to_push[start][finish])
        if flow_to_push < 1e-9:
            break
        current_result[start][finish] += flow_to_push
        is_edge_valid = np.array([edge_validation(edge, edge_dim_lowerbound)
                                  for edge in graph.edges], dtype=bool)
        visited_vertices = np.zeros(nodes_count, dtype=bool)
        _, path = find_path_with_lowerbounded_capacities(start, finish, is_edge_valid,
                                                                     graph, adj, visited_vertices)

        current_path = []
        for edge_idx in path[::-1]:
            unordered_edge_idx = edge_idx
            if unordered_edge_idx >= len(graph.edges):
                unordered_edge_idx -= len(graph.edges)
            current_edge = graph.edges[unordered_edge_idx]
            graph.edges[unordered_edge_idx] = current_edge._replace(dim=current_edge.dim - flow_to_push)
            current_path.append(unordered_edge_idx)
        solution.append(OneIterationPathes(signal_size=flow_to_push,
                                           start=start, finish=finish,
                                           pathes_edge_idx=[current_path]))
    resulted_flow = check_solution(initial_graph, goal, solution)
    return np.amax(goal - resulted_flow), resulted_flow, solution
