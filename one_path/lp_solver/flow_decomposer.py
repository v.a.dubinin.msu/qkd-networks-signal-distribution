import numpy as np
from typing import List, Dict, Tuple
from dataclasses import dataclass

from qkd_networks_signal_distribution.utils import OneIterationPathes


@dataclass
class FlowAdjEdge:
    v: int
    capacity: float
    edge_idx: int


def find_path(v: int, finish: int, capacity_lowerbound: float,
              adj_matrix: Dict[int, List[FlowAdjEdge]], visited_nodes: np.ndarray) -> Tuple[bool, List[FlowAdjEdge]]:
    if v == finish:
        return True, []
    visited_nodes[v] = 1
    for flow_edge in adj_matrix[v]:
        to = flow_edge.v
        if (flow_edge.capacity < capacity_lowerbound) or (visited_nodes[to] == 1):
            continue
        is_found, result = find_path(to, finish, capacity_lowerbound, adj_matrix, visited_nodes)
        if is_found:
            return True, [flow_edge] + result
    return False, None


def decompose_flow_to_pathes(source: int, nodes_count: int, edges: List[Tuple[str, str]], capacities: List[float]) -> List[OneIterationPathes]:
    adj_matrix = {}
    for edge_idx, (edge, capacity) in enumerate(zip(edges, capacities)):
        u, v = int(edge[0]), int(edge[1])
        if not (u in adj_matrix):
            adj_matrix[u] = []
        adj_matrix[u].append(FlowAdjEdge(v=v, capacity=capacity, edge_idx=edge_idx))
    result = []
    while True:
        left, right = 0, np.amax(capacities)
        while right - left > 1e-6:
            middle = (left + right) / 2
            visited_nodes = np.zeros(nodes_count + 1, dtype=int)
            is_found, _ = find_path(source, nodes_count, middle, adj_matrix, visited_nodes)
            if is_found:
                left = middle
            else:
                right = middle
        if left < 1e-6:
            break
        visited_nodes = np.zeros(nodes_count + 1, dtype=int)
        is_found, flow_edges_path = find_path(source, nodes_count, left, adj_matrix, visited_nodes)
        assert is_found
        pathes_edge_idx = []
        for i, flow_edge in enumerate(flow_edges_path):
            flow_edge.capacity -= left
            if i < len(flow_edges_path) - 1:
                assert flow_edge.edge_idx < len(edges) - nodes_count
                edges_count = (len(edges) - nodes_count) // 2
                unordered_edge_idx = flow_edge.edge_idx
                assert unordered_edge_idx < 2 * edges_count
                if unordered_edge_idx >= edges_count:
                    unordered_edge_idx -= edges_count
                pathes_edge_idx.append(unordered_edge_idx)
            if i == len(flow_edges_path) - 1:
                assert flow_edge.edge_idx >= len(edges) - nodes_count
        finish = flow_edges_path[-2].v
        result.append(OneIterationPathes(signal_size=left, start=source, finish=finish, pathes_edge_idx=[pathes_edge_idx]))
    return result
