from typing import Tuple, List

import numpy as np
from pulp import *

from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes
from qkd_networks_signal_distribution.one_path.lp_solver.flow_decomposer import decompose_flow_to_pathes
from qkd_networks_signal_distribution.solution_checker import check_solution


def get_adj_revadj_linear(graph: Graph) -> Tuple[dict, dict]:
    nodes_count = graph.nodes_count

    def add_edge(adj, u, v):
        if not (u in adj):
            adj[u] = []
        adj[u].append(v)

    adj_matrix = {}
    revadj_matrix = {}
    for edge in graph.edges:
        add_edge(adj_matrix, edge.u, edge.v)
        add_edge(adj_matrix, edge.v, edge.u)
        add_edge(revadj_matrix, edge.v, edge.u)
        add_edge(revadj_matrix, edge.u, edge.v)
    for v in range(nodes_count):
        add_edge(adj_matrix, v, nodes_count)
        add_edge(revadj_matrix, nodes_count, v)

    return adj_matrix, revadj_matrix


def goal_checking_linear_programming_solution(graph: Graph, goal: np.ndarray) -> Tuple[float,
                                                                                       np.ndarray,
                                                                                       List[OneIterationPathes]]:
    nodes_count = graph.nodes_count

    prob = LpProblem("QKD", LpMaximize)

    flow_sources = [str(i) for i in range(nodes_count)]
    flow_edges = [(str(e.u), str(e.v)) for e in graph.edges] + \
                 [(str(e.v), str(e.u)) for e in graph.edges] + \
                 [(str(i), str(nodes_count)) for i in range(nodes_count)]
    final_edges = [(str(source), (str(v), str(nodes_count))) for source in range(nodes_count) for v in
                   range(nodes_count)]

    vars = LpVariable.dicts("Flow", (flow_sources, flow_edges), 0, None)

    prob += (
        lpSum([vars[source][edge] for (source, edge) in final_edges]),
        "Goal function",
    )

    for edge in graph.edges:
        prob += (
            lpSum([vars[str(source)][(str(edge.u), str(edge.v))] for source in range(nodes_count)]
                  + [vars[str(source)][(str(edge.v), str(edge.u))] for source in range(nodes_count)]) <= edge.dim,
            "Edge_{0}_capacity_limitation".format((str(edge.u), str(edge.v)))
        )

    for s in range(nodes_count):
        for v in range(nodes_count):
            prob += (
                vars[str(s)][(str(v), str(nodes_count))] <= goal[s][v],
                "{0}_Edge_{1}_capacity_limitation".format(str(s), (str(v), str(nodes_count)))
            )

    adj_matrix, revadj_matrix = get_adj_revadj_linear(graph)

    for s in range(nodes_count):
        for u in range(nodes_count):
            if u == s:
                continue
            prob += (
                lpSum(
                    [vars[str(s)][(str(u), str(v))] for v in adj_matrix[u]] + [-vars[str(s)][(str(v), str(u))] for v in
                                                                               revadj_matrix[u]]) == 0,
                "Flow with source {0} conservation in vertex {1}".format(s, u)
            )

    prob.solve()

    solution = [[value(vars[s][e]) for e in flow_edges] for s in flow_sources]

    decomposed_solution = []
    for s in range(nodes_count):
        pathes = decompose_flow_to_pathes(s, nodes_count, flow_edges, solution[s])
        decomposed_solution.extend(pathes)

    resulted_flow = check_solution(graph, goal, decomposed_solution)
    return np.amax(goal - resulted_flow), resulted_flow, decomposed_solution


def linear_programming_solution(graph: Graph, goal: np.ndarray) -> Tuple[float, np.ndarray, List[OneIterationPathes]]:
    left_border = 0
    right_border = np.amax(goal)
    while right_border - left_border > 1e-6:
        middle = (left_border + right_border) / 2
        residual_goal, _, _ = goal_checking_linear_programming_solution(graph, np.maximum(0, goal - middle))
        if residual_goal < 1e-5:
            right_border = middle
        else:
            left_border = middle
    resudial_goal, resulted_flow, solution = goal_checking_linear_programming_solution(
        graph,
        np.maximum(0, goal - right_border)
    )
    assert resudial_goal < 1e-5, "Check error: wrong resudial_goal - expected < 1e-5, found {0}".format(resudial_goal)
    return resudial_goal + right_border, resulted_flow, solution
