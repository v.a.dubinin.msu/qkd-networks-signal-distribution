from typing import Tuple, List
import copy
import numpy as np
import random

from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes
from qkd_networks_signal_distribution.graph_generators import random_connected_graph


def get_adj_generation(graph: Graph, rng: random.Random) -> dict:
    def add_edge(adj, u, idx):
        if not (u in adj):
            adj[u] = []
        adj[u].append(idx)

    adj_matrix = {}

    for idx, edge in enumerate(graph.edges):
        add_edge(adj_matrix, edge.u, idx)
        add_edge(adj_matrix, edge.v, idx)

    for key in adj_matrix:
        rng.shuffle(adj_matrix[key])

    return adj_matrix


def get_other_vertex(graph: Graph, edge_idx: int, first_vertex: int) -> int:
    return graph.edges[edge_idx].u + graph.edges[edge_idx].v - first_vertex


def find_path(current_vertex: int, finish: int, graph: Graph, adj: dict, visited_vertices: np.ndarray) \
        -> List[int]:
    visited_vertices[current_vertex] = 1
    if current_vertex == finish:
        return []
    for edge_idx in adj[current_vertex]:
        next_vertex = get_other_vertex(graph, edge_idx, current_vertex)
        if not visited_vertices[next_vertex]:
            path_tail = find_path(next_vertex, finish, graph, adj, visited_vertices)
            if path_tail is not None:
                return [edge_idx] + path_tail
    return None


def find_random_path(graph: Graph, start: int, finish: int, flow_size: int, seed: int) -> Tuple[Graph, List[int]]:
    rng = np.random.RandomState(seed)
    nodes_count = graph.nodes_count
    adj = get_adj_generation(graph, rng)
    visited_vertices = np.zeros(nodes_count)
    path = find_path(start, finish, graph, adj, visited_vertices)
    for edge_idx in path:
        current_edge = graph.edges[edge_idx]
        graph.edges[edge_idx] = current_edge._replace(dim=current_edge.dim + flow_size)
    return graph, path


#
# Here we use only graph's topology as input and change graph's edges capacities in output.
#

def generate_goal(graph: Graph, generation_pathes_count: int, one_path_upper_bound: int, seed: int) \
                  -> Tuple[Graph, np.ndarray, List[OneIterationPathes]]:
    rng = np.random.RandomState(seed)
    nodes_count = graph.nodes_count
    goal = np.zeros((nodes_count, nodes_count))
    for edge_idx in range(len(graph.edges)):
        graph.edges[edge_idx] = graph.edges[edge_idx]._replace(dim=0)
    pathes_list = []
    for _ in range(generation_pathes_count):
        st, fn = rng.choice(np.arange(nodes_count), size=2, replace=False)
        if st > fn:
            st, fn = fn, st
        flow_size = rng.randint(one_path_upper_bound) + 1
        graph, path_idxes = find_random_path(graph, st, fn, flow_size, rng.randint(2 ** 31 - 1))
        pathes_list.append(OneIterationPathes(signal_size=flow_size, start=st, finish=fn, pathes_edge_idx=[path_idxes]))
        goal[st][fn] += flow_size
    return graph, goal, pathes_list


def generate_problem(nodes_count: int, edges_to_nodes_ratio: float, generation_pathes_count_constant: float,
                     seed: int = 0) -> Tuple[Graph, np.ndarray, List[OneIterationPathes]]:
    edges_count = int(edges_to_nodes_ratio * nodes_count + 0.5)
    generation_pathes_count = int(generation_pathes_count_constant * nodes_count * nodes_count + 0.5)
    one_path_upper_bound = 10
    graph = random_connected_graph(nodes_count=nodes_count, edges_count=edges_count, max_dim=1, seed=seed,
                                   fixed_dim=False, log_dim=False)
    graph, goal, pathes_list = generate_goal(graph, generation_pathes_count=generation_pathes_count,
                                             one_path_upper_bound=one_path_upper_bound, seed=seed)
    return graph, goal, pathes_list
