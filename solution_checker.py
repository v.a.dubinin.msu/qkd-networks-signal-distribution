import numpy as np
from typing import List

from qkd_networks_signal_distribution.utils import Graph, Edge, OneIterationPathes


def check_solution(graph: Graph, goal: np.ndarray, solution: List[OneIterationPathes],
                   pathes_count: int = 1) -> np.ndarray:
    nodes_count = graph.nodes_count
    edge_flows = np.zeros(len(graph.edges), dtype=float)
    resulted_flow = np.zeros((nodes_count, nodes_count), dtype=float)

    for one_iteration_pathes in solution:
        nodes_usage_count = np.zeros(nodes_count, dtype=int)
        all_pathes = one_iteration_pathes.pathes_edge_idx
        assert len(all_pathes) == pathes_count, "Check error: wrong pathes count - expected {0}, found {1}" \
            .format(pathes_count, len(all_pathes))
        current_flow = one_iteration_pathes.signal_size
        start = one_iteration_pathes.start
        finish = one_iteration_pathes.finish
        resulted_flow[start][finish] += current_flow

        assert start != finish, "Check error: path start {0} is equal to finish".format(start)
        for path in all_pathes:
            current_vertex = start
            for edge_idx in path:
                edge_flows[edge_idx] += current_flow
                u, v = graph.edges[edge_idx].u, graph.edges[edge_idx].v
                assert (current_vertex == u) or (current_vertex == v), "Check error: edges don't form a path"
                new_vertex = u + v - current_vertex
                if new_vertex != finish:
                    nodes_usage_count[new_vertex] += 1
                current_vertex = new_vertex
            assert current_vertex == finish, "Check error: path has a wrong end - expected {0}, found {1}" \
                .format(finish, current_vertex)
        assert np.amax(nodes_usage_count) <= 1, "Check error: vertex is used in pathes more than one time\n{0}" \
            .format(nodes_usage_count)
    for flow, edge in zip(edge_flows, graph.edges):
        assert flow < edge.dim + 1e-2, "Check error: edge's flow is greater than capacity - " \
                                       "capacity {0}, flow {1}".format(edge.dim, flow)

    print("All flow checks are successful")
    return resulted_flow
