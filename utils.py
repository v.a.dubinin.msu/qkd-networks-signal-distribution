from collections import namedtuple


Graph = namedtuple('Graph', ['nodes_count', 'edges'])
Edge = namedtuple('Edge', ['id', 'u', 'v', 'dim'])

OneIterationPathes = namedtuple('OneIterationPathes', ['signal_size', 'start', 'finish', 'pathes_edge_idx'])